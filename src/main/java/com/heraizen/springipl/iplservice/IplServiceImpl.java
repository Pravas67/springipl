package com.heraizen.springipl.iplservice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import com.heraizen.springipl.domain.Player;
import com.heraizen.springipl.domain.Team;
import com.heraizen.springipl.dto.PlayerDTO;
import com.heraizen.springipl.dto.RoleCountDTO;
import com.heraizen.springipl.dto.TeamAmountDTO;
import com.heraizen.springipl.dto.TeamDTO;
import com.heraizen.springipl.dto.TeamLableDTO;

public class IplServiceImpl implements IplService {
	List<Team> teamList = null;

	private static transient IplService iplServiceObj = null;

	private IplServiceImpl() {
	}

	public static IplService getInstance() {
		if (iplServiceObj == null) {
			synchronized (IplServiceImpl.class) {
				if (iplServiceObj == null) {
					iplServiceObj = new IplServiceImpl();
				}
			}
		}
		return iplServiceObj;
	}

	@Override
	public TeamLableDTO getTeamLabels() {
		List<String> labels = teamList.stream().map(Team::getLabel).collect(Collectors.toList());
		return TeamLableDTO.builder().labels(labels).build();
	}

	@Override
	public List<PlayerDTO> getPlayersByTeam(String label) {
		List<PlayerDTO> playerByLabel = new ArrayList<>();
		teamList.forEach(t -> {
			if (t.getLabel().equalsIgnoreCase(label)) {
				t.getPlayers().forEach(p -> {
					PlayerDTO playerDTO = PlayerDTO.builder().name(p.getName()).price(p.getPrice()).role(p.getRole())
							.build();
					playerByLabel.add(playerDTO);
				});

			}
		});
		if (playerByLabel.isEmpty()) {
			throw new IllegalArgumentException("Please Enter valid label");
		}
		return playerByLabel;
	}

	@Override
	public List<RoleCountDTO> getRoleCountByteam(String teamName) {
		List<PlayerDTO> playerByLabel = new ArrayList<>();
		teamList.forEach(t -> {
			if (t.getLabel().equalsIgnoreCase(teamName)) {
				t.getPlayers().forEach(p -> {
					PlayerDTO playerDTO = PlayerDTO.builder().name(p.getName()).price(p.getPrice()).role(p.getRole())
							.build();
					playerByLabel.add(playerDTO);
				});
			}
		});
		List<RoleCountDTO> roleCountDTO = new ArrayList<RoleCountDTO>();
		if (playerByLabel.size() != 0) {

			long batsman = playerByLabel.stream().filter(p -> p.getRole().equalsIgnoreCase("Batsman")).count();
			long wicketKeeper = playerByLabel.stream().filter(p -> p.getRole().equalsIgnoreCase("Wicket Keeper"))
					.count();
			long allRounder = playerByLabel.stream().filter(p -> p.getRole().equalsIgnoreCase("All-Rounder")).count();
			long bowler = playerByLabel.stream().filter(p -> p.getRole().equalsIgnoreCase("Bowler")).count();

			roleCountDTO.add(RoleCountDTO.builder().role("Batsman").count(batsman).build());
			roleCountDTO.add(RoleCountDTO.builder().role("All-Rounder").count(allRounder).build());
			roleCountDTO.add(RoleCountDTO.builder().role("Bowler").count(bowler).build());
			roleCountDTO.add(RoleCountDTO.builder().role("Wicket Keeper").count(wicketKeeper).build());

		}
		return roleCountDTO;
	}

	@Override
	public List<PlayerDTO> getPlayersByTeamAndRole(String teamName, String role) {
		List<List<Player>> listPlayers = teamList.stream().filter(t -> t.getLabel().equalsIgnoreCase(teamName))
				.map(Team::getPlayers).collect(Collectors.toList());
		List<PlayerDTO> allPlayers = new ArrayList<PlayerDTO>();
		for (List<Player> players : listPlayers) {
			for (Player player : players) {
				if (player.getRole().equalsIgnoreCase(role)) {
					allPlayers.add(PlayerDTO.builder().name(player.getName()).price(player.getPrice())
							.role(player.getRole()).build());
				}
			}
		}

		return allPlayers;
	}

	@Override
	public List<TeamDTO> getTeamDetails() {
		List<TeamDTO> teamDetails = new ArrayList<TeamDTO>();
		teamList.forEach(team -> {
			teamDetails.add(TeamDTO.builder().name(team.getName()).coach(team.getCoach()).home(team.getHome())
					.label(team.getLabel()).build());
		});
		return teamDetails;
	}

	@Override
	public List<TeamAmountDTO> getAmountSpentByTeam(String label) {
		List<List<Player>> listPlayers = teamList.stream().filter(t -> t.getLabel().equalsIgnoreCase(label))
				.map(Team::getPlayers).collect(Collectors.toList());
		TeamAmountDTO teamAmountDTO = null;
		List<TeamAmountDTO> teamAmountDtoList = new ArrayList<>();
		for (List<Player> players : listPlayers) {
			double amount = players.stream().mapToDouble(p -> p.getPrice()).sum();
			teamAmountDTO = TeamAmountDTO.builder().label(label).amount(amount).build();
			teamAmountDtoList.add(teamAmountDTO);
		}
		return teamAmountDtoList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PlayerDTO> getPlayersSortByPrice() {
		List<PlayerDTO> playerList = new ArrayList<>();

		teamList.forEach(t -> {
			t.getPlayers().forEach(p -> {
				PlayerDTO playerDTO = PlayerDTO.builder().name(p.getName()).price(p.getPrice()).role(p.getRole())
						.build();
				playerList.add(playerDTO);
			});
		});
		Collections.sort(playerList);
		return playerList;
	}

	@Override
	public Map<String, PlayerDTO> getMaxPaidPlayerForEachRole() {
		Map<String, PlayerDTO> playersList = new HashMap<>();
		List<List<Player>> listPlayers = teamList.stream().map(Team::getPlayers).collect(Collectors.toList());
		List<Player> allPlayers = new ArrayList<>();
		for (List<Player> players : listPlayers) {
			allPlayers.addAll(players);

		}
		System.out.println("allPlayers.."+allPlayers.size());
		Player maxPaidForBatsman = allPlayers.stream().filter(p -> p.getRole().equalsIgnoreCase("Batsman"))
				.max(Comparator.comparing(Player::getPrice)).get();
		Player maxPaidForBowler = allPlayers.stream().filter(p -> p.getRole().equalsIgnoreCase("Bowler"))
				.max(Comparator.comparing(Player::getPrice)).get();
		Player maxPaidForAllRounder = allPlayers.stream().filter(p -> p.getRole().equalsIgnoreCase("All-Rounder"))
				.max(Comparator.comparing(Player::getPrice)).get();
		Player maxPaidForWicketKeeper = allPlayers.stream().filter(p -> p.getRole().equalsIgnoreCase("Wicket Keeper"))
				.max(Comparator.comparing(Player::getPrice)).get();
		System.out.println("maxPaidForBatsman" + maxPaidForBatsman.getName());
		playersList.put("Batsman", PlayerDTO.builder().name(maxPaidForBatsman.getName())
				.role(maxPaidForBatsman.getRole()).price(maxPaidForBatsman.getPrice()).build());
		playersList.put("Bowler", PlayerDTO.builder().name(maxPaidForBowler.getName()).role(maxPaidForBowler.getRole())
				.price(maxPaidForBowler.getPrice()).build());
		playersList.put("All-Rounder", PlayerDTO.builder().name(maxPaidForAllRounder.getName())
				.role(maxPaidForAllRounder.getRole()).price(maxPaidForAllRounder.getPrice()).build());
		playersList.put("Wicket Keeper", PlayerDTO.builder().name(maxPaidForWicketKeeper.getName())
				.role(maxPaidForWicketKeeper.getRole()).price(maxPaidForWicketKeeper.getPrice()).build());

		System.out.println(playersList);
		return playersList;
	}

	@Override
	public List<Team> addTeam(List<Team> teams) {
		teamList = teams;
		return teamList;
	}

	@Override
	public void deleteAllTeams() {
		teamList.clear();

	}

}
