package com.heraizen.springipl.iplservice;

import java.util.List;
import java.util.Map;

import com.heraizen.springipl.domain.Team;
import com.heraizen.springipl.dto.PlayerDTO;
import com.heraizen.springipl.dto.RoleCountDTO;
import com.heraizen.springipl.dto.TeamAmountDTO;
import com.heraizen.springipl.dto.TeamDTO;
import com.heraizen.springipl.dto.TeamLableDTO;

public interface IplService {
	TeamLableDTO getTeamLabels();

	List<PlayerDTO> getPlayersByTeam(String teamName);

	List<RoleCountDTO> getRoleCountByteam(String teamName);

	List<PlayerDTO> getPlayersByTeamAndRole(String teamName, String role);

	List<TeamDTO> getTeamDetails();

	List<TeamAmountDTO> getAmountSpentByTeam(String label);

	List<PlayerDTO> getPlayersSortByPrice();

	Map<String, PlayerDTO> getMaxPaidPlayerForEachRole();

	public List<Team> addTeam(List<Team> teamList);

	public void deleteAllTeams();
}
