package com.heraizen.springipl.domain;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Player implements Comparable<Player> {
//	private String id;
	private String name;
	private double price;
	private String role;

	@Override
	public int compareTo(Player o) {
		double comparePrice = ((Player) o).getPrice();
		return (int) (this.price - comparePrice);
	}
}
