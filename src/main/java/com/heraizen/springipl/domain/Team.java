package com.heraizen.springipl.domain;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Team {
	private String city;
	private String coach;
	private String home;
	private String name;
	private String label;
//	private String id;
	private List<Player> players;
}
