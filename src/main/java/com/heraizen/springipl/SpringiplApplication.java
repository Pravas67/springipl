package com.heraizen.springipl;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.heraizen.springipl.domain.Team;
import com.heraizen.springipl.util.JsonReader;

@SpringBootApplication
public class SpringiplApplication implements CommandLineRunner {
	private final String FILE_NAME = "iplinfo.json";

	public static void main(String[] args) {
		SpringApplication.run(SpringiplApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

//		List<Team> teamList = JsonReader.readData(FILE_NAME);
	}

}
