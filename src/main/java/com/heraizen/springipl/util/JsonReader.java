package com.heraizen.springipl.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.heraizen.springipl.domain.Team;

public class JsonReader {
	private JsonReader() {

	}

	public static List<Team> readData(String fileName) {
		return readTeamByJson(fileName);
	}

	private static List<Team> readTeamByJson(String FILE_NAME) {
		List<Team> teams = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			teams = mapper.readValue(new File(FILE_NAME), new TypeReference<List<Team>>() {
			});
		} catch (IOException e) {

			e.printStackTrace();
		}
		System.out.println("teams" + teams);
		return teams;
	}
}
