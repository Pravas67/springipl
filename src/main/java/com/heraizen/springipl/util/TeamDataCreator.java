package com.heraizen.springipl.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.heraizen.springipl.domain.Player;
import com.heraizen.springipl.domain.Team;

@Component
public class TeamDataCreator {
	public List<Team> createTeamList() {
		List<Team> teamList = new ArrayList<Team>();
		List<Player> players1 = new ArrayList<Player>();
		players1.add(Player.builder().name("Rohit Sharma (R)").role("Batsman").price(150000000).build());
		players1.add(Player.builder().name("Suryakumar Yadav (R)").role("Batsman").price(32000000).build());
		players1.add(Player.builder().name("Hardik Pandya (R)").role("All-Rounder").price(110000000).build());
		players1.add(Player.builder().name("Jasprit Bumrah (R)").role("Bowler").price(70000000).build());
		Team team1 = Team.builder().city("Mumbai, Maharashtra").home("Wankhede Stadium, Mumbai")
				.coach("Mahela Jaywardene").label("MI").name("Mumbai Indians").players(players1).build();
		teamList.add(team1);
		List<Player> players2 = new ArrayList<Player>();
		players2.add(Player.builder().name("Shreyas Iyer (R)").role("Batsman").price(70000000).build());
		players2.add(Player.builder().name("Chris Woakes").role("All-Rounder").price(15000000).build());
//		players.add(Player.builder().name("Ravichandran Ashwin (R)").role("Bowler").price(76000000).build());
		Team team2 = Team.builder().city("Delhi, NCR").home("Feroz Shah Kotla Ground").coach("Ricky Ponting")
				.label("DC").name("Delhi Capitals").players(players2).build();
		teamList.add(team2);
		return teamList;
	}
}
