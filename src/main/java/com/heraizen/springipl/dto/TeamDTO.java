package com.heraizen.springipl.dto;

import java.util.List;

import com.heraizen.springipl.domain.Player;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TeamDTO {
	private String city;
	private String coach;
	private String home;
	private String name;
	private String label;
//	private String id;
	private List<Player> players;
}
