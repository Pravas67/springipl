package com.heraizen.springipl.iplservice;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.heraizen.springipl.domain.Team;
import com.heraizen.springipl.util.TeamDataCreator;

@SpringBootTest
public class IplServiceTest {
	public static List<Team> teamList = new ArrayList<Team>();
	private static TeamDataCreator teamDataCreator = new TeamDataCreator();

	private IplService iplService = null;

//	@BeforeAll
//	public static void getTeamList() {
//		teamList = teamDataCreator.createTeamList();
//	}

	@BeforeEach
	public void addTeamList() {
		iplService = IplServiceImpl.getInstance();
		teamList = teamDataCreator.createTeamList();
		iplService.addTeam(teamList);
	}

	@AfterEach
	public void clearTeamList() {
		iplService.deleteAllTeams();
	}

	@Test
	@DisplayName("Get all team label test")
	public void testGetAllTeamLabels() {
		List<String> labels = new ArrayList<String>();
		labels.add("MI");
		labels.add("DC");
		Assertions.assertEquals(labels, iplService.getTeamLabels().getLabels());
	}

	@Test
	@DisplayName("Get all players by team label test")
	public void testGetPlayersByTeam() {
		Assertions.assertEquals("Rohit Sharma (R)", iplService.getPlayersByTeam("MI").get(0).getName());
	}

	@Test
	@DisplayName("Get all players by team label test for invalid label")
	public void testGetPlayersByTeamWithInvalidLabel() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> iplService.getPlayersByTeam("CL"));
	}

	@Test
	@DisplayName("get role count by team test")
	public void testGetRoleCountByteam() {
		Assertions.assertAll(
				() -> Assertions.assertEquals(2, iplService.getRoleCountByteam("MI").get(0).getCount(),
						() -> "Batsman"),
				() -> Assertions.assertEquals(1, iplService.getRoleCountByteam("MI").get(1).getCount(),
						() -> "All-rounder"),
				() -> Assertions.assertEquals(0, iplService.getRoleCountByteam("MI").get(3).getCount(),
						() -> "Wicket-keeper"));
	}

	@Test
	@DisplayName("Get players by team and role test")
	public void testGetAllPlayersByTeamAndLabel() {
		Assertions.assertAll(
				() -> Assertions.assertEquals(2, iplService.getPlayersByTeamAndRole("MI", "Batsman").size(),
						() -> "Batsman"),
				() -> Assertions.assertEquals(0, iplService.getPlayersByTeamAndRole("DC", "Bowler").size(),
						() -> "All-rounder"),
				() -> Assertions.assertEquals("Chris Woakes",
						iplService.getPlayersByTeamAndRole("DC", "All-Rounder").get(0).getName(),
						() -> "Wicket-keeper"));
	}

	@Test
	@DisplayName("Get team details test")
	public void testGetTeamDetails() {
		Assertions.assertAll(() -> Assertions.assertEquals(2, iplService.getTeamDetails().size(), () -> "All teams"),
				() -> Assertions.assertEquals("Mahela Jaywardene", iplService.getTeamDetails().get(0).getCoach(),
						() -> "Coach"),
				() -> Assertions.assertEquals("DC", iplService.getTeamDetails().get(1).getLabel(),
						() -> "Dc label test"));
	}

	@Test
	@DisplayName("Get amount spent by team test")
	public void testGetAmountSpentByTeam() {
		Assertions.assertAll(
				() -> Assertions.assertEquals(3.62E8, iplService.getAmountSpentByTeam("MI").get(0).getAmount(),
						() -> "MI team"),
				() -> Assertions.assertEquals(8.5E7, iplService.getAmountSpentByTeam("DC").get(0).getAmount(),
						() -> "DC team"));
	}

	@Test
	@DisplayName("Get players sort by price test")
	public void testGetPlayersSortByPrice() {
		Assertions
				.assertAll(
						() -> Assertions.assertEquals("Rohit Sharma (R)",
								iplService.getPlayersSortByPrice().get(iplService.getPlayersSortByPrice().size() - 1)
										.getName(),
								() -> "Highest paid player"),
						() -> Assertions.assertEquals("Chris Woakes",
								iplService.getPlayersSortByPrice().get(0).getName(), () -> "Lowest paid player"));
	}

	@Test
	@DisplayName("Get max paid player for each role test")
	public void testGetMaxPaidPlayerForEachRole() {
		Assertions.assertAll(
				() -> Assertions.assertEquals("Rohit Sharma (R)",
						iplService.getMaxPaidPlayerForEachRole().get("Batsman").getName(), () -> "Batsman"),
				() -> Assertions.assertEquals("Hardik Pandya (R)",
						iplService.getMaxPaidPlayerForEachRole().get("All-rounder").getName(), () -> "All-rounder"),
				() -> Assertions.assertEquals(null,
						iplService.getMaxPaidPlayerForEachRole().get("Wicket-keeper").getName(), () -> "Wicket-keeper"),
				() -> Assertions.assertEquals("Jasprit Bumrah (R)",
						iplService.getMaxPaidPlayerForEachRole().get("Bowler").getName(), () -> "Bowler"));
	}
}
